# Super Hero Joke Teller #

Have your favorite super heroes tell jokes to each other with this easy to use drop down hero selector!
To get a new joke, just select another hero!

Pitch: Sit down and watch your favourite super heroes battle against each other trying to sound the toughest! Based on the original idea of simulating a real-time conversation, this super hero joke teller has even Chuck Norris's approval with popular Chuck Norris jokes replaced by either the superhero or familiar name.

## Running The Application ##

This app was bootstrapped with create-react-app. Node version: 12^; npm version: 5.6^; In the project directory, you can run:

### `npm start`

Runs the app in the development mode.
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.
You will also see any lint errors in the console.