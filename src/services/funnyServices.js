export const getSuperHeroInfo = async (superhero) => {
    return fetch("https://superhero-search.p.rapidapi.com/?hero=" + superhero, {
	"method": "GET",
	"headers": {
		"x-rapidapi-host": "superhero-search.p.rapidapi.com",
		"x-rapidapi-key": "287b488474msh066df0915c2144bp15ec25jsn2ed725b2298d"
	}
    })
    .then(response => {
        return response.json();
    })
    .catch(err => {
        console.log(err);
    });
};

export const getChuckNorrisJoke = async () => {
    return fetch("https://api.chucknorris.io/jokes/random", {"method": "GET"})
    .then(response => {
        return response.json();
    })
    .catch(err => {
        console.log(err);
    });
};

export const getFunnyCombo = async (superhero, comboLogic) => {
    return getSuperHeroInfo(superhero)
    .then(heroResponse => {
        // console.log("hero resp ", heroResponse)
        return getChuckNorrisJoke()
        .then(chuckResponse => {
            // console.log("chuck joke resp ", chuckResponse)
            if(comboLogic){
                return {
                    joke: comboLogic(heroResponse, chuckResponse),
                    img: heroResponse.images.sm
                } 
            } else {
                return {};
            }
        })
    })
};

