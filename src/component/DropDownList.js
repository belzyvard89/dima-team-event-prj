import React from 'react';
import Characters from '../data/Characters.json';
import './DropDownList.css';


export default function DropDownList({ onChangeHandler }) {

    return (
        <select className="dropDownList" onChange={onChangeHandler}>
            {
            Characters.names.map((name, i) => <option key={i} value={name}>{name}</option>)
            }
        </select>
    );
};