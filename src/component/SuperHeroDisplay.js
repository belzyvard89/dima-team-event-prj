import React from 'react';
import './SuperHeroDisplay.css'

export default function SuperHeroDisplay(props) {
    return (
        <div className="SuperHeroDisplay">
            <div className="superHeroDisplayHeading">{props.name}</div>
            <div className="superHeroDisplayJoke">{props.joke}</div>
            <img src={props.src} className="superHeroDisplayImage"/>
        </div>
    );
}