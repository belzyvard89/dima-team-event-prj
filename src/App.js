import React, {useState, useEffect} from 'react';
import logo from './logo.svg';
import './App.css';
import DropDownList from './component/DropDownList'
import SuperHeroDisplay from './component/SuperHeroDisplay';
import {getFunnyCombo} from './services/funnyServices';
import {assembleJoke1} from './jokeBuilder/jokes';

function App() {

  const [init1, setInit1] = useState(false)
  const [init2, setInit2] = useState(false)
  const [superheroOne, setSuperheroOne] = useState('Spiderman')
  const [jokeOne, setJokeOne] = useState('')
  const [superheroImgOne, setSuperheroImgOne] = useState('');
  const [superheroTwo, setSuperheroTwo] = useState('Spiderman')
  const [jokeTwo, setJokeTwo] = useState('')
  const [superheroImgTwo, setSuperheroImgTwo] = useState('');
  
  useEffect( () => {
    if(superheroOne && superheroOne!== ''){
      getFunnyCombo(superheroOne, assembleJoke1)
      .then(response => {
        // console.log("resp is ", response.joke)
        setJokeOne(response.joke);
        setSuperheroImgOne(response.img);
        setJokeTwo(response.joke);
        setSuperheroImgTwo(response.img);
      })
    }
  }, []);

  useEffect( () => {
    if(init1 && superheroOne && superheroOne!== ''){
      getFunnyCombo(superheroOne, assembleJoke1)
      .then(response => {
        // console.log("resp is ", response.joke)
        setJokeOne(response.joke);
        setSuperheroImgOne(response.img);
      })
    } else {
      setInit1(true);
    }

  }, [superheroOne]);

  useEffect( () => {
    if(init2 && superheroTwo && superheroTwo!== ''){
      getFunnyCombo(superheroTwo, assembleJoke1)
      .then(response => {
        // console.log("resp is ", response.joke)
        setJokeTwo(response.joke);
        setSuperheroImgTwo(response.img);
      })
    } else {
      setInit2(true);
    }

  }, [superheroTwo]);

  return (
    <div className="App">
      <div className="dropDownListContainer">
        <DropDownList onChangeHandler={ (evt) => {setSuperheroOne(evt.target.value)}}></DropDownList>
        <DropDownList onChangeHandler={ (evt) => {setSuperheroTwo(evt.target.value)}}></DropDownList>
      </div>
      <div style={{display: 'flex'}}>
        <SuperHeroDisplay name={superheroOne} joke={jokeOne} src={superheroImgOne}></SuperHeroDisplay>
        <SuperHeroDisplay name={superheroTwo} joke={jokeTwo} src={superheroImgTwo}></SuperHeroDisplay>
      </div>
    </div>
  );
}

export default App;
